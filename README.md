# Thing Tracker Application
**Target: CT100X, CT102X Series Devices**<br/>
**Version: v2.0.9.2a**<br/>
**Release Date: 2024/02/14**

This CP-Flex application provides basic functions useful for tracking assets (things).  
It has the same basic functionality as the ped-tracker without button UI and emergency features.

## Key Features  
The tracker application is designed to provide useful tracking and event 
information as an individual goes about their daily activities.

1. Configurable motion activated location data collection 
2. Cloud configurable tracking settings / report
3. Caching of messages when out of range
4. Cloud controlled tracking activation / deactivation
5. Battery status events and report every 5%
6. Cloud configurable temperature monitoring, default configuration is as follows:
   * Samples temperature every minute
   * Reports statistics every two (2) hours
   * Reports threshold event whenever temperature changes more than 5 degrees C.
   * Reports threshold event whenever temperature changes more than 2.5 degrees C in less than a minute.
7. Cloud controlled location measurement configuration control for (future).
8. Network Link Checks every 4 hours.

# User Interface 
This application is meant to be used with both the Flex Tracker (CT100X) and Micro Tracker (CT102X) series of tags.   Certain UI capabilities will vary depending upon the device support. 

## Button Descriptions  (CT1000 only)

The following table summarizes the user interface for this application.  See the sections
below for more detailed information regarding each button action.  

| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Locate Now**                      | single (1) <br>quick press        | button #1          | Initiates an immediate location request  |  
| **Battery Status**                  | single (1) <br>quick press        | button #2          | Indicates battery level <br> [*See Battery Indicator*](#battery-indicator)  |  
| **Network Reset**                   | hold > 15<br>seconds              | any button or both | Network reset device      |
| **Factory Reset**                   | hold > 25<br>seconds              | any button or both | Resets the network and configuration to factory defaults for the application.     |
 
## LED Descriptions

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads
* **LED #2** - Lower LED, closest to charger pads (CT100X series only)

| **Description**                |                **Indication**               |   **LEDS**  |
|--------------------------------|:-------------------------------------------:|:-----------:|
| **Device Reset**               |       Green blink <br>three (3) times       |     both    |
| **Locate Initiated**           |        Green blink <br>two (2) times        |    LED #1   |
| **Battery Charging**           | Orange slow blink every<br>ten (10) seconds |    LED #1   |
| **Battery Fully Charged**      |  Green slow blink every<br>ten (10) seconds |    LED #1   |
| **Invalid Input**              |  Red blink <br>three (3) times quickly      |    LED #1   |

See status indicators below for additional LED signaling when requesting battery status.

### Battery Indicator  (CT1000 only)

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description                                     |
|----------|-------------|-------------------------------------------------|
|  1 red   |   < 10%     | Battery level is critical,  charge immediately. |
|  1 green |  15% - 30%  | Battery low  less than 30% remaining.           |
|  2 green |  30% - 50%  | Battery is about 40% charged.                    |
|  3 green |  50% - 70%  | Battery is about 60% charged.                    |
|  4 green |  70% - 90%  | Battery  is about 80% charged.                   |
|  5 green |  > 90%      | Battery is fully charged.                       |

## General Operating Mode
General operating mode is entered when the device boots. The tracking mode is on and 
the tag reports location in coarse mode every 5 minutes.  

*Note:  If the tag is on the charger when booting, the entry to general operating mode will be delayed
2 minutes.*

---
*Copyright 2019-2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
