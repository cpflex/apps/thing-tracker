/**
*  Name:  ConfigTemperatureSensor.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
* This module implements the Impulse Sensor configuration.
**/
#include <OcmNidDefinitions.i>

//NID Command Map, by default these map to ped_tracker protocol spec.
//If your protocol is different, update the NID mappings.
const NIDMAP: {
	NM_TEMP_REPORT		= _:NID_TemperatureReport, 
	NM_TEMP_STATSRPT 	= _:NID_TemperatureStatisticsrpt,
	NM_TEMP_TRIGGERRPT 	= _:NID_TemperatureTriggerrpt,
	NM_TEMP_TRIGGERDEF  = _:NID_TemperatureTriggerdef,
	NM_TEMP_TRIGGERSCLEAR 	= _:NID_TemperatureCleartriggers,
	NM_TEMP_DTREPORT 		= _:NID_TemperatureConfigDtreport,
	NM_TEMP_DTSTATS 		= _:NID_TemperatureConfigDtstats,
	NM_TEMP_DTSAMPLING 		= _:NID_TemperatureConfigDtsampling,
	NM_TEMP_FLAGS 			= _:NID_TemperatureConfigFlags,
	NM_TEMP_CONFIG= _:NID_TemperatureConfig,
};

const  TEMP_MAX_TRIGGERS = 4;
const  TEMP_DTREPORT = 60;   // daily reporting in minutes
const  TEMP_DTSTATS  = 120;  // statistics reporting in minutes.
const  TEMP_DTSAMPLE = 60;   // sampling in seconds
const  TEMP_FLAGS	 = 3;    // flags

//const  MsgPriority:		IMPULSE_MSGPRIORITY	= MP_high;	//Message priority.	
//const  MsgCategory:		IMPULSE_MSGCATEGORY	= MC_alert;	//Message category.

//Comment out if not using defaults and managed by App.
const TEMP_DEFINE_DEFAULT_TRIGGERS = 1;  //Sets the Default Triggers.
const TEMP_DEFINE_AUTOENABLE = 1;        //Sets Auto enable if configuration is defined on reset.