/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* Cora Thing Tracker Protocol Schema
*  nid:        CoraThingTracker
*  uuid:       d7327874-fac4-4f96-8f61-f72b18258e33
*  ver:        1.0.0.0
*  date:       2023-09-19T05:04:59.632Z
*  product: Cora Tracking CT10XX
* 
*  Copyright 2023 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_LocConfigPmode = 1,
	NID_TemperatureConfigDtsampling = 2,
	NID_SystemLogAll = 3,
	NID_LocConfigPmodeDefault = 4,
	NID_TrackConfig = 5,
	NID_TemperatureCleartriggers = 6,
	NID_TemperatureConfigDtreport = 7,
	NID_SystemLogDisable = 8,
	NID_PowerBattery = 9,
	NID_TemperatureConfigFlags = 10,
	NID_LocConfigPtech = 11,
	NID_SystemReset = 12,
	NID_PowerChargerCritical = 13,
	NID_TrackConfigNomintvl = 14,
	NID_SystemConfigPollintvl = 15,
	NID_SystemLog = 16,
	NID_TemperatureTriggerrpt = 17,
	NID_TrackMode = 18,
	NID_TemperatureConfig = 19,
	NID_SystemLogAlert = 20,
	NID_TemperatureReport = 21,
	NID_LocConfigPmodePrecise = 22,
	NID_LocConfigPtechBle = 23,
	NID_LocConfigPtechWifi = 24,
	NID_TrackModeActive = 25,
	NID_LocConfig = 26,
	NID_LocConfigPmodeMedium = 27,
	NID_PowerCharger = 28,
	NID_LocConfigPmodeCoarse = 29,
	NID_TrackModeDisabled = 30,
	NID_TrackConfigEmrintvl = 31,
	NID_SystemErase = 32,
	NID_LocConfigPtechWifible = 33,
	NID_LocConfigPtechAutomatic = 34,
	NID_TemperatureTriggerdef = 35,
	NID_PowerChargerCharging = 36,
	NID_SystemLogInfo = 37,
	NID_TemperatureStatisticsrpt = 38,
	NID_TrackConfigAcquire = 39,
	NID_TrackConfigInactivity = 40,
	NID_SystemLogDetail = 41,
	NID_PowerChargerCharged = 42,
	NID_SystemConfig = 43,
	NID_PowerChargerDischarging = 44,
	NID_TrackModeEnabled = 45,
	NID_TemperatureConfigDtstats = 46
};
