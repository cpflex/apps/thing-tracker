﻿# Thing-Tracker Application Release Notes

### V2.0.9.2a - 240214
1. Updated CT1XXX firmware reference to v2.5.3.2a
- Enhanced System Debug Features

### V2.0.9.1a - 240213
1. Updated CT1XXX firmware reference to v2.5.3.1a
- Enhanced System Debug Features

### V2.0.9.0a - 240208
1. Updated CT1XXX firmware reference to v2.5.3.0a
- Enhanced Reset Messages

### V2.0.8.1a - 240205
1. Updated CT1XXX firmware reference to v2.5.2.1a

### V2.0.8.0a - 240203
1. Updated CT1XXX firmware reference to v2.5.2.0a
 - Enhanced System Debug Features

### V2.0.7.0a - 240123
1. Updated CT1XXX firmware reference to v2.5.1.0a
 - Bug fixes for BVT / Telem

### V2.0.6.1a - 240116
1. Updated CT1XXX firmware reference to v2.5.0.2a
 - Bug fixes to Archive
 - Changed Telem Unavailable retry from 7 seconds to 30 seconds

### V2.0.6.0a - 240109
1. Updated CT1XXX firmware reference to v2.5.0.0a
 - Fixed various system issues
 
### V2.0.5.3a - 231206
1. Updated v1 Library version to synchronize with other alpha's

### V2.0.5.2a - 231128
1. Updated CT1XXX firmware reference to v2.4.4.2a
 - Fixed TimeSync bug (KP-350)

### V2.0.5.1a - 231117
1. Firmware reference to v2.4.4.1a

### V2.0.5.0a - 231116
1. Firmware reference to v2.4.4.0a
 - v1.04 LoRaWAN 
 - US915 SB2 / ADR Disabled config
 - EU868 standard band / ADR enabled config
 - Various accelerometer fixes
2. Changed Aquire Motion Threshold from 50mG to 100mG
3. Changed Inactivity Motion Threshold from 100mG to 400mG

### V2.0.4.0a_104 - 231109
1. Firmware Reference v2.4.2.0a (v1.04 radio stack)
   * Various RTC Kernel Fixes
   * NETWORK RESET operation now rejoins instead of clearing NVM params

### V2.0.3.1a_104 - 231025
1. Firmware Reference v2.4.1.0a (v1.04 radio stack)
   * correct typo in Telemetry Library (CT1020 firmware only)

### V2.0.3.0a_104 - 231024
1. Firmware Reference v2.4.1.0a (v1.04 radio stack)
   * Implements refactored FlexTelem with LinkCheck support
   * Default uplink retry scheduler changed
   * After 3 sequential uplink comm failures, TelemState changes to unavailable

### V2.0.2.0a - 231011
1. Firmware Reference v2.4.0.0a
  - Implements LoRaWAN v1.04
  - Full Band US915 ADR enabled
  - Standard EU band ADR enabled

### V2.0.1.3 - 230928
1. Firmware Reference v2.3.0.1
 - Fixes Timercheck in main app 

### V2.0.1.2 - 230921
1. FIX: V2.0.1.0 Was not encoding messages correctly according to the Thing Tracker V1.0.0.0 OCMV1 schema .   Updated the build and tested.  
   Now working again.
   
### V2.0.1.0_SENET - 230920
1. Firmware Reference v2.3.0.0_s
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.
4. US915 full band / ADR enabled for CT1000 devices only (CT1020 supports US915 SB2 / ADR disabled)

### V2.0.1.0 - 230919
1. Firmware Reference v2.3.0.0
2. Release build

### V2.0.0.0a - 230918
1. Rewrite of app to support standard library modules.
2. Now implements its own protocol API: //hub.cpflex.tech/cora/interfaces/ocmv1/thing-tracker.
3. Updated User interface
4. Added Temperature module.  
5. Updated MotionTracking module to be consistent with personnel tracker config.
6. Firmware Reference v2.2.5.1a
7. Flex Platform: v1.4.0.2

### V1.1.1.4a - 230817
1. Firmware Reference v2.2.2.2k
 - Fixes Flash configs, deep sleep, charger LED indicator, power consumption, unlocks SPI Flash on CT1020's with prior firmware, fixes LED boot indicator bug

### V1.1.1.1a - 230810
1. Firmware Reference v2.2.1.1k
 - KP-Stable2 alpha release
 - Fixed SPI Flash power consumption and deep-sleep logic

### V1.1.1.0 - 230719
1. Firmware Reference v2.2.1.0
 - Production release of KP_STABLE1
 - Enable tracking by default in app-init

### V1.1.0.2a - 230707
1. Firmware Reference v2.2.0.2k KP_STABLE1
 - Many fixes, see firmware notes
 - Uses OPL v1.0.0.4 requires new N100 and Production tools to be compatible

### V1.1.0.1a - 230628
1. Firmware Reference v2.2.0.1k KP_STABLE1
 - Foundation: Fixed callback invoke self-queue bug

### v1.1.0.0a 230616 ###
1. Firmware Reference v2.2.0.0k KP_STABLE1
 - Foundation Bug fixes: Queue, Callback subsystems
 - Added TimerTick API to PAWN-API

### v1.0.6.1a 230616 ###
  * bundle updated to v2.1.5.1k
v1.0.6.0a 230616
  * bundle updated to v2.1.5.0k
    - fixes major RTC and opulink bugs

### v1.0.5.0a 230510 ###
  * bundle updated to v2.1.1.0a
  * Major platform updates and DUX12 fix for manufacturing

### v1.0.4.2a 230426 ###
  * bundle updated to v2.1.0.2a
  * Modified BVT LED Behavior

### v1.0.4.1a 230425 ###
  * bundle updated to v2.1.0.1a
  * New radio support and other system enhancements (still supporting DUX12)

### V1.0.3.1a 230417 ###
  * bundle updated to v2.0.5.1a
  * Fixed DUX12 BVT BSP for intial CT1020 manufacturing

### V1.0.3.0a 230413 ###
  * CT1020 firmware bundle updated to v2.0.5.0a
  * Adds prototype DUX12 Accelerometer support
  * Uses latest libraries with optional Telemetry boot join and fixes

### V1.0.2.4 230208 ###
* CT1020 firmware bundle updated to v2.0.3.4

### V1.0.2.3 230207 ###
* CT1020 firmware bundle updated to v2.0.3.3

### V1.0.2.2 230207 ###
* CT1020 firmware bundle updated to v2.0.3.2

### V1.0.2.1 230207 ###
  * CT1020 firmware bundle updated to v2.0.3.1

### V1.0.2.0 230204 ###
  * CT1020 firmware bundle updated to v2.0.3.0
  * Initial BVT Production Release

### V1.0.1.0a 221214 ###
  * CT1020 firmware bundle updated to v2.0.1.0a

### V1.0.0.0a 221013 ###
- Initial Release
  * CT1020 firmware bundle to v2.0.0.0a
  * Uses V1 Library v1.2.8.0
  * Derived from ped_tracker V2.2.5.4 for V2 Stack.
   

---
*Copyright 2019-2022, Codepoint Technologies, Inc.* 
*All Rights Reserved*