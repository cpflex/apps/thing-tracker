@ECHO OFF
if [%1]==[] goto usage

set specfile="hub_spec.json"
set tag=%1
ECHO Pushing Thing Tracker Application version %1
cphub push -v -l -s %specfile% cpflexapp "./thing-tracker.bin" cora/apps/tracking/thing-tracker:%tag%
goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
